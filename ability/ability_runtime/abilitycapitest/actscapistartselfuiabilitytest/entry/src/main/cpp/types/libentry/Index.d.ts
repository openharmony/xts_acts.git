/*
 * Copyright (c) 2024-2025 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

export const wantNdk_0100: () => number;
export const wantNdk_0200: () => number;
export const wantNdk_0300: () => number;
export const wantNdk_0400: () => number;
export const wantNdk_0500: () => number;
export const wantNdk_0600: () => number;
export const wantNdk_0700: () => number;
export const wantNdk_0800: () => number;
export const wantNdk_0900: () => number;
export const wantNdk_1000: () => number;
export const wantNdk_1100: () => number;
export const wantNdk_1200: () => number;
export const wantNdk_1300: () => number;
export const wantNdk_1400: () => number;
export const wantNdk_1500: () => number;
export const wantNdk_1600: () => number;
export const wantNdk_1700: () => number;
export const wantNdk_1800: () => number;
export const wantNdk_1900: () => number;
export const wantNdk_2000: () => number;
export const wantNdk_2100: () => number;
export const wantNdk_2200: () => number;
export const wantNdk_2300: () => number;
export const wantNdk_2400: () => number;
export const wantNdk_2500: () => number;
export const ndkStartAbility_0100: () => number;
export const ndkStartAbility_0200: () => number;
export const ndkStartAbility_0300: () => number;
export const ndkStartAbility_0400: () => number;
export const ndkStartAbility_0500: () => number;
export const ndkStartAbility_0900: () => number;
export const ndkStartAbility_1000: () => number;
export const ndkStartAbility_1100: () => number;
export const ndkStartAbility_1200: () => number;
export const ndkStartAbility_1600: () => number;